.. Cypress Over The Air documentation master file, created by
   sphinx-quickstart on Wed May 20 16:59:18 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cypress Over The Air's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. doxygenindex::
   :project: CypressOverTheAir

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. doxygenfunction:: cy_ota_agent_start
   :project: CypressOverTheAir
